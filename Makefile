init:
	pip install -r requirements.txt

test:
	nosetests tests 
	
gui:
	pyuic5 main_form_design.ui -o main_form_design.py

build:
	printf "ENVIRONMENT = 'prod'\n" > environment.py
	~/venvs/venv3/bin/python3.7 -OO -m PyInstaller --clean evrimgui.bin.spec
	printf "ENVIRONMENT = 'devel'\n" > environment.py
	
.PHONY: init test gui build
