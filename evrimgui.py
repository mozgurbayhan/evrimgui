# -*- coding: utf-8 -*-
import os
import sys

sys.path.append(os.path.realpath(os.path.dirname(__file__)))

from PyQt5.QtWidgets import QApplication

from main_form import MainForm

__author__ = 'ozgur'
__creation_date__ = '12.10.2019' '22:34'

if __name__ == '__main__':
    app = QApplication(sys.argv)
    form = MainForm()
    form.show()
    app.exec_()
    sys.exit()
