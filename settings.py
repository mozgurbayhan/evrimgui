# -*- coding: utf-8 -*-
import sys

import os
from pathlib import Path

from environment import ENVIRONMENT

__author__ = 'ozgur'
__creation_date__ = '13.10.2019' '00:50'

if ENVIRONMENT=="devel":
    BASE_DIR = os.path.dirname(os.path.abspath(__file__))
    HOME_DIR = os.path.join(BASE_DIR, "devhome")
else:
    # noinspection PyUnresolvedReferences
    BASE_DIR=sys._MEIPASS
    HOME_DIR=os.path.join(str(Path.home()),".evrim")


ASSETS_DIR = os.path.join(BASE_DIR, "assets")
PLUGIN_DIR = os.path.join(HOME_DIR, "plugins")
CACHE_DIR = os.path.join(HOME_DIR, "cache")
