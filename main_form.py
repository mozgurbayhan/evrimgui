# -*- coding: utf-8 -*-

import json
import os
from PyQt5.QtCore import QTimer
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QMainWindow, QSystemTrayIcon
from pathlib import Path

from main_form_design import Ui_MainWindow
from page_widget import PageWidget, PageInfo
from settings import PLUGIN_DIR, ASSETS_DIR, CACHE_DIR

__author__ = 'ozgur'
__creation_date__ = '12.10.2019' '22:37'


class MainForm(QMainWindow, Ui_MainWindow):
    def __init__(self):
        super(self.__class__, self).__init__()
        self.pagewidgetsdict = {}
        # ##### QT PART #####
        self.setupUi(self)
        self.tray_icon = QSystemTrayIcon(self)
        self.tray_icon.setIcon(QIcon(os.path.join(ASSETS_DIR, "icon-evolution-80.png")))
        self.tray_icon.setToolTip("Evrim")
        self.tray_icon.show()
        # noinspection PyUnresolvedReferences
        self.tray_icon.activated.connect(self.event_tray_clicked)
        self.main_gui_tricks()
        self.initialize()

        self.alarmtimer = QTimer(self)
        # noinspection PyUnresolvedReferences
        self.alarmtimer.timeout.connect(self.event_alarmtimer_tick)
        self.alarmtimer.start(10 * 1000)

        self.alarmtimer = QTimer(self)
        # noinspection PyUnresolvedReferences
        self.alarmtimer.timeout.connect(self.event_collecttimer_tick)
        self.alarmtimer.start(2 * 1000)
        self.hasalarms = False

    def _log(self, logstr):
        self.txt_log.insertPlainText(logstr + "\n")

    def _get_messages_js(self, plugindir: str) -> str:
        try:
            with open(os.path.join(ASSETS_DIR, "evrim.js"), "r") as ofile:
                pretext = ofile.read()

            with open(os.path.join(plugindir, "webview.js"), "r") as ofile:
                jstext = ofile.read().strip()
            if "module.exports" in jstext:
                parsetxt = jstext.split("getMessages()")[1]
                opc = 0
                cpc = 0
                startpoint = 0
                endpoint = 0
                for i, char in enumerate(parsetxt):
                    if char == "{":
                        if opc == 0:
                            startpoint = i
                        opc += 1
                    elif char == "}":
                        cpc += 1
                    if opc > 0 and opc == cpc:
                        endpoint = i
                        break
                rettext = "function getMessages()" + parsetxt[startpoint:endpoint + 1]
                rettext = rettext.replace("Franz", "Evrim")
                rettext += "\ngetMessages();\n Evrim.badgecount;"
                rettext = pretext + rettext
            else:
                rettext = jstext
        except Exception as ex:
            dirname = os.path.basename(os.path.dirname(plugindir))
            logstr = "Cant load plugin js:{} with exception:{}".format(dirname, str(ex))
            self._log(logstr)
            rettext = ""
        return rettext

    def _read_plugin(self, plugindir: str) -> PageInfo:

        # noinspection PyBroadException
        try:
            msg_js = self._get_messages_js(plugindir)
            iconpath = os.path.join(plugindir, "icon.svg")
            if not os.path.isfile(iconpath):
                raise FileNotFoundError(iconpath)

            with open(os.path.join(plugindir, "package.json"), "r") as ofile:
                retval = PageInfo(json.loads(ofile.read(), encoding="utf-8"), iconpath, msg_js)
        except Exception:
            retval = None
        return retval

    # noinspection PyUnusedLocal
    def event_tray_clicked(self, reason):
        if self.isVisible():
            self.hide()
        else:
            self.show()

    def main_gui_tricks(self):
        self.tabWidget.setCurrentIndex(0)
        self.showMaximized()

    def initialize(self):
        Path(PLUGIN_DIR).mkdir(parents=True, exist_ok=True)
        Path(CACHE_DIR).mkdir(parents=True, exist_ok=True)

        self.tabWidget.setTabIcon(0, QIcon(os.path.join(ASSETS_DIR, "icon-settings-64.png")))
        self.tabWidget.setTabIcon(1, QIcon(os.path.join(ASSETS_DIR, "icon-evolution-80.png")))
        self.btn_refresh.setIcon(QIcon(os.path.join(ASSETS_DIR, "icon-refresh-64.png")))
        self.btn_back.setIcon(QIcon(os.path.join(ASSETS_DIR, "icon-back-64.png")))

        plugindirs = [os.path.join(PLUGIN_DIR, x) for x in os.listdir(PLUGIN_DIR) if
                      os.path.join(PLUGIN_DIR, x)]

        for pdir in plugindirs:
            pageinfo = self._read_plugin(pdir)
            self._get_messages_js(pdir)
            if pageinfo:
                lastindex = self.tabWidget.count()
                self.pagewidgetsdict[pageinfo.pid] = PageWidget(self.tabWidget, tabindex=lastindex,
                                                                pageinfo=pageinfo)

    def event_alarmtimer_tick(self):
        for key, pagewidget in self.pagewidgetsdict.items():
            pagewidget.run_alarms_js()

    def event_collecttimer_tick(self):
        total = 0

        for key, pagewidget in self.pagewidgetsdict.items():
            try:
                acount = int(pagewidget.eg_alarmcount)
            except (TypeError, ValueError):
                acount = 0
            total += acount

        if self.hasalarms and total == 0:
            self.tray_icon.setIcon(QIcon(os.path.join(ASSETS_DIR, "icon-evolution-80.png")))
            self.hasalarms = False
        elif self.hasalarms is False and total > 0:
            self.tray_icon.setIcon(QIcon(os.path.join(ASSETS_DIR, "icon-evolution-red-80.png")))
            self.hasalarms = True
        else:
            pass
