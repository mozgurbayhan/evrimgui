# -*- coding: utf-8 -*-

from PyQt5 import QtCore, QtGui, QtWidgets, QtWebEngineWidgets
import os

from settings import ASSETS_DIR, CACHE_DIR

__author__ = 'ozgur'
__creation_date__ = '12.10.2019' '22:38'


class PageInfo:
    def __init__(self, infodict: dict, icon_path, msg_js: str):
        if not isinstance(infodict, dict):
            raise TypeError
        self.pid = infodict["id"]
        self.name = infodict["name"]
        self.version = infodict.get("version", "")
        self.description = infodict.get("description", "")
        self.author = infodict.get("author", "")
        self.service_url = infodict["config"]["serviceURL"]

        self.icon_path = icon_path
        self.alarmscript = msg_js


class PageWidget:
    def __init__(self, tabwidget: QtWidgets.QTabWidget, tabindex: int, pageinfo: PageInfo):
        self.eg_alarmcount = 0
        self.eg_pageinfo = pageinfo
        self.eg_tabindex = tabindex
        self.tabWidget = tabwidget
        # noinspection PyArgumentList
        self.tab_page = QtWidgets.QWidget()
        self.tab_page.setObjectName("tab_" + self.eg_pageinfo.pid)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.tab_page)
        self.verticalLayout_2.setObjectName("verticalLayout_" + self.eg_pageinfo.pid)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setSizeConstraint(QtWidgets.QLayout.SetDefaultConstraint)
        self.horizontalLayout.setSpacing(1)
        self.horizontalLayout.setObjectName("horizontalLayout_" + self.eg_pageinfo.pid)
        self.btn_refresh = QtWidgets.QPushButton(self.tab_page)
        sizepolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizepolicy.setHorizontalStretch(0)
        sizepolicy.setVerticalStretch(0)
        sizepolicy.setHeightForWidth(self.btn_refresh.sizePolicy().hasHeightForWidth())
        self.btn_refresh.setSizePolicy(sizepolicy)
        self.btn_refresh.setMinimumSize(QtCore.QSize(32, 32))
        self.btn_refresh.setMaximumSize(QtCore.QSize(32, 32))
        self.btn_refresh.setAutoFillBackground(False)
        self.btn_refresh.setText("")
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(os.path.join(ASSETS_DIR, "icon-refresh-64.png")),
                        QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btn_refresh.setIcon(icon1)
        self.btn_refresh.setFlat(True)
        self.btn_refresh.setObjectName("btn_refresh_" + self.eg_pageinfo.pid)
        # noinspection PyArgumentList
        self.horizontalLayout.addWidget(self.btn_refresh)
        self.btn_back = QtWidgets.QPushButton(self.tab_page)
        sizepolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizepolicy.setHorizontalStretch(0)
        sizepolicy.setVerticalStretch(0)
        sizepolicy.setHeightForWidth(self.btn_back.sizePolicy().hasHeightForWidth())
        self.btn_back.setSizePolicy(sizepolicy)
        self.btn_back.setMinimumSize(QtCore.QSize(32, 32))
        self.btn_back.setMaximumSize(QtCore.QSize(32, 32))
        self.btn_back.setText("")
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap(os.path.join(ASSETS_DIR, "icon-back-64.png")),
                        QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btn_back.setIcon(icon2)
        self.btn_back.setFlat(True)
        self.btn_back.setObjectName("btn_back_" + self.eg_pageinfo.pid)
        # noinspection PyArgumentList
        self.horizontalLayout.addWidget(self.btn_back)
        spacer_item = QtWidgets.QSpacerItem(10, 10, QtWidgets.QSizePolicy.Expanding,
                                            QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacer_item)
        self.verticalLayout_2.addLayout(self.horizontalLayout)
        self.webEngineView = QtWebEngineWidgets.QWebEngineView(self.tab_page)
        cachepath = os.path.join(CACHE_DIR, self.eg_pageinfo.pid)
        useragent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) " \
                    "Chrome/77.0.3865.120 Safari/537.36"
        self.webEngineView.page().profile().setHttpUserAgent(useragent)
        self.webEngineView.page().profile().setCachePath(cachepath)
        self.webEngineView.page().profile().setPersistentStoragePath(cachepath)
        self.webEngineView.setUrl(QtCore.QUrl(self.eg_pageinfo.service_url))
        self.webEngineView.setObjectName("webEngineView_" + self.eg_pageinfo.pid)
        # noinspection PyArgumentList
        self.verticalLayout_2.addWidget(self.webEngineView)
        pageicon = QtGui.QIcon()
        pageicon.addPixmap(QtGui.QPixmap(self.eg_pageinfo.icon_path), QtGui.QIcon.Normal,
                           QtGui.QIcon.Off)
        self.tabWidget.addTab(self.tab_page, pageicon, "")
        self.tabWidget.setTabToolTip(tabindex, self.eg_pageinfo.name)

        # noinspection PyUnresolvedReferences
        self.btn_refresh.clicked.connect(self.refresh_page)
        # noinspection PyUnresolvedReferences
        self.btn_back.clicked.connect(self.webEngineView.back)

    def run_js(self, jscript: str, callback_function=None):
        try:
            if not callback_function:
                callback_function = self.cb_jsreturn
            self.webEngineView.page().runJavaScript(jscript, callback_function)
        except Exception as ex:
            print(str(ex), self.eg_pageinfo.name)

    def refresh_page(self):
        self.webEngineView.setUrl(QtCore.QUrl(self.eg_pageinfo.service_url))

    @staticmethod
    def cb_jsreturn(return_value):
        print(return_value)

    def run_alarms_js(self):
        self.run_js(self.eg_pageinfo.alarmscript, self.cb_alarmreturn)

    def cb_alarmreturn(self, return_value):

        tabindex = self.tabWidget.indexOf(self.tab_page)

        try:
            self.eg_alarmcount = int(return_value)
        except (ValueError, TypeError):
            self.eg_alarmcount = 0

        if self.eg_alarmcount > 0:
            self.tabWidget.setTabText(tabindex, str(self.eg_alarmcount))
        else:
            self.tabWidget.setTabText(tabindex, "")
